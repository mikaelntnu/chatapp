package no.ntnu.tollefsen.chat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import no.ntnu.tollefsen.chat.domain.Conversation;
import no.ntnu.tollefsen.chat.domain.Message;


public class ConversationAdapter extends RecyclerView.Adapter<ConversationAdapter.ConversationViewHolder>{
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    OnClickListener listener = position -> {};
    List<Conversation> conversations;

    public ConversationAdapter() {
        this.conversations = new ArrayList<>();
    }

    public List<Conversation> getConversations() {
        return conversations;
    }

    public void setConversations(List<Conversation> conversations) {
        this.conversations = conversations;
        notifyDataSetChanged();
    }

    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public ConversationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.conversation,parent,false);
        return new ConversationViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ConversationViewHolder holder, int position) {
        String username = "";
        String text = "";
        String date = "";
        Conversation conversation = getConversations().get(position);
        if(conversation.getMessages().size() > 0) {
            Message message = conversation.getMessages().get(conversation.getMessages().size()-1);
            username = message.getSender().getFirstName();
            text = message.getText();
            Date created = message.getCreated();
            if(created != null) {
                date = DATE_FORMAT.format(created);
            }
        }
        holder.username.setText(username);
        holder.text.setText(text);
        holder.date.setText(date);
    }

    @Override
    public int getItemCount() {
        return getConversations().size();
    }

    interface OnClickListener {
        void onClick(int position);
    }

    class ConversationViewHolder extends RecyclerView.ViewHolder {

        TextView username;
        TextView text;
        TextView date;

        public ConversationViewHolder(View view) {
            super(view);
            view.setOnClickListener(v -> listener.onClick(getAdapterPosition()));
            this.username = view.findViewById(R.id.username);
            this.text = view.findViewById(R.id.text);
            this.date = view.findViewById(R.id.date);
        }
    }
}
