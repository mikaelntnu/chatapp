package no.ntnu.tollefsen.chat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import no.ntnu.tollefsen.chat.domain.User;

public class UserAdapter  extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {
    UserAdapter.OnClickListener listener = position -> {};

    List<User> users;
    List<User> selectedUsers = new ArrayList<>();

    public UserAdapter() {
        this.users = new ArrayList<>();
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
        this.selectedUsers = new ArrayList<>();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.user,parent,false);
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        User user = getUsers().get(position);
        holder.firstname.setText(user.getFirstName());
        holder.lastname.setText(user.getLastName());

        // Set background if selected
        holder.view.setBackgroundColor(selectedUsers.contains(user) ?
            ResourcesCompat.getColor(holder.view.getResources(), R.color.selectedBackground, null) :
            ResourcesCompat.getColor(holder.view.getResources(), android.R.color.transparent, null));
    }

    @Override
    public int getItemCount() {
        return getUsers().size();
    }


    public List<User> getSelectedUsers() {
        return selectedUsers;
    }

    public void setOnClickListener(UserAdapter.OnClickListener listener) {
        this.listener = listener;
    }

    protected void onClick(int position) {
        User user = getUsers().get(position);

        if(selectedUsers.contains(user)) {
            selectedUsers.remove(user);
        } else {
            selectedUsers.add(user);
        }
        notifyItemChanged(position);

        listener.onClick(position);
    }

    interface OnClickListener {
        void onClick(int position);
    }

    class UserViewHolder extends RecyclerView.ViewHolder {
        View view;
        TextView firstname;
        TextView lastname;

        public UserViewHolder(View view) {
            super(view);
            this.view = view;
            view.setOnClickListener(v -> onClick(getAdapterPosition()));
            this.firstname = view.findViewById(R.id.firstname);
            this.lastname = view.findViewById(R.id.lastname);
        }
    }
}
