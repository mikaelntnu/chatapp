package no.ntnu.tollefsen.chat;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import no.ntnu.tollefsen.chat.domain.Conversation;
import no.ntnu.tollefsen.chat.domain.User;
import no.ntnu.tollefsen.chat.tasks.CallbackTask;

public class RecipientActivity extends AppCompatActivity {
    UserAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipient);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        setTitle(R.string.new_conversation);

        Button startchat = findViewById(R.id.startchat);
        startchat.setEnabled(false);

        adapter = new UserAdapter();
        adapter.setOnClickListener(position -> {
            TextView recipients = findViewById(R.id.recipients);
            recipients.setText(Tools.getRecipients(adapter.getSelectedUsers()));
            startchat.setEnabled(adapter.getSelectedUsers().size() > 0);
        });

        RecyclerView recyclerView = findViewById(R.id.users);
        GridLayoutManager layoutManager = new GridLayoutManager(this,3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new MarginDecoration(10));

        // Load users from server
        new CallbackTask<Void, Void, List<User>>(
            voids -> ChatService.getSingleton().loadAllUsers(),
            users -> adapter.setUsers(users), this::onException
        ).execute();

        // Create conversation when button clicked
        startchat.setOnClickListener(view ->
            new CallbackTask<Void, Void,Conversation>(
                voids -> ChatService.getSingleton().createConversation(adapter.getSelectedUsers()),
                conversation -> {
                    ChatService.getSingleton().setSelectedConversation(conversation);
                    this.finish();
                    startActivity(new Intent(RecipientActivity.this, MessageActivity.class));
                }, this::onException
            ).execute()
        );
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }


    /**
     * Display errormessage to user
     *
     * @param throwable
     */
    protected void onException(Throwable throwable) {
        Log.i("RecipientActivity","Error", throwable);
        Snackbar.make(findViewById(android.R.id.content),
                "Failed to communicate with server " + throwable.getMessage(), Snackbar.LENGTH_LONG)
                .setAction("Action", null)
                .show();
    }
}
