package no.ntnu.tollefsen.chat.tasks;


public class CallbackTask<Params, Progress, Result> extends AbstractAsyncTask<Params, Progress, Result> {
    AbstractAsyncTask.OnBackground<Params,Result> onBackground;

    public CallbackTask(AbstractAsyncTask.OnBackground<Params,Result> onBackground,
                        AbstractAsyncTask.OnPostExecute<Result> onPostExecute,
                        OnException onException) {
        super(onPostExecute,onException);
        this.onBackground = onBackground;
    }


    @Override
    protected Result doInBackground(Params... params) {
        if(onBackground != null) {
            try {
                return onBackground.doInBackground(params);
            } catch (Exception e) {
                setException(e);
            }
        }
        return null;
    }
}
