package no.ntnu.tollefsen.chat.tasks;

import android.util.JsonReader;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

import no.ntnu.tollefsen.chat.ChatService;
import no.ntnu.tollefsen.chat.domain.Message;
import no.ntnu.tollefsen.chat.domain.Photo;

public class PostMessageTask extends AbstractAsyncTask<Message, Void, List<Message>> {
    private final static String CRLF = "\r\n";
    private final String boundary = "*****" + System.currentTimeMillis() + "*****";

    boolean firstFormField = true;

    public PostMessageTask(OnPostExecute<List<Message>> onPostExecute,
                           OnException onException) {
        super(onPostExecute, onException);
    }


    private void addFormField(BufferedWriter bw, String name, String value) throws IOException {
        if(!firstFormField) {
            bw.write(CRLF);
        }

        firstFormField = false;
        bw.write("--");
        bw.write(boundary);
        bw.write(CRLF);
        bw.write("Content-Disposition: form-data; name=\"" + name + "\"");
        bw.write(CRLF);
        bw.write("Content-Type: text/plain; charset=UTF-8");
        bw.write(CRLF);
        bw.write(CRLF);
        bw.write(value);
        bw.flush();
    }

    @Override
    protected List<Message> doInBackground(Message... messages) {
        List<Message> result = new ArrayList<>();

        for(Message msg : messages) {
            try {
                // Setup request
                HttpURLConnection con = ChatService.getSingleton().getSecureConnection(ChatService.SEND_MESSAGE_URL);
                con.setDoInput(true);
                con.setDoOutput(true); // POST
                con.setUseCaches(false);
                con.setRequestProperty("Connection", "Keep-Alive");
                con.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

                // Write message & conversationid
                OutputStream os = con.getOutputStream();
                BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, StandardCharsets.UTF_8));
                addFormField(bw,"conversationid", Long.toString(msg.getConversationId()));
                addFormField(bw,"message", msg.getText());


                // Write photos
                for(Photo photo : msg.getPhotos()) {
                    if(photo.getFile() != null) {
                        // Start content wrapper
                        bw.write("--" + boundary + CRLF);
                        bw.write("Content-Type: " + URLConnection.guessContentTypeFromName(photo.getFilename()));
                        bw.write(CRLF);
                        bw.write("Content-Disposition: form-data; name=\"image\"; filename=\"" +
                                photo.getFilename() + "\"");
                        bw.write(CRLF);
                        bw.write("Content-Transfer-Encoding: binary");
                        bw.write(CRLF);
                        bw.write(CRLF);
                        bw.flush();

                        // Copy photo-stream
                        int len;
                        try (InputStream is = new BufferedInputStream(new FileInputStream(photo.getFile()))) {
                            byte[] buff = new byte[2048];
                            while ((len = is.read(buff)) != -1) {
                                os.write(buff, 0, len);
                            }
                        }

                        // End and flush buffers
                        bw.write(CRLF);
                        bw.flush();
                    }
                }

                // End content wrapper
                bw.write(CRLF);
                bw.write("--");
                bw.write(boundary);
                bw.write("--");
                bw.write(CRLF);
                bw.flush();
                bw.close();
                con.getOutputStream().close();

                // Get response
                if(con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(new InputStreamReader(con.getInputStream(), StandardCharsets.UTF_8));
                    JsonReader jr = new JsonReader(br);
                    result.add(ChatService.getSingleton().loadMessage(jr));
                }
                con.disconnect();
            } catch (IOException e) {
                Log.e("PostMessageTask", "doInBackground: ", e);
            }
        }

        return result;
    }
}