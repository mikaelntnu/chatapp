package no.ntnu.tollefsen.chat.tasks;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import no.ntnu.tollefsen.chat.ChatService;
import no.ntnu.tollefsen.chat.domain.Conversation;


public class ConversationLoader extends AbstractAsyncTask<Void, Void, List<Conversation>> {
    public ConversationLoader(OnPostExecute<List<Conversation>> onPostExecute, OnException onException) {
        super(onPostExecute, onException);
    }

    @Override
    protected List<Conversation> doInBackground(Void... nothingHereMoveAlong) {
        try {
            return ChatService.getSingleton().loadConversations();
        } catch (IOException e) {
            setException(e);
        }

        return Collections.emptyList();
    }
}
