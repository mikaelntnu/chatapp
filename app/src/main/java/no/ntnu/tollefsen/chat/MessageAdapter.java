package no.ntnu.tollefsen.chat;

import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import no.ntnu.tollefsen.chat.domain.Message;
import no.ntnu.tollefsen.chat.domain.Photo;

public class MessageAdapter  extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {
    public static final int LEFT_VIEW = 0;
    public static final int RIGHT_VIEW = 1;


    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    OnClickListener listener = position -> {};

    List<Message> messages;

    public MessageAdapter() {
        this.messages = new ArrayList<>();
    }

    public List<Message> getMessages() {
        return messages;
    }

    public void setMessages(List<Message> messages) {
        this.messages = messages;
        notifyDataSetChanged();
    }

    public void addMessages(List<Message> messages) {
        this.messages.addAll(messages);
        notifyDataSetChanged();
    }


    public void setOnClickListener(OnClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        Message message = messages.get(position);
        if(ChatService.getSingleton().isUser(message.getSender().getUserid()))
            return RIGHT_VIEW;
        else
            return LEFT_VIEW;
    }

    @NonNull
    @Override
    public MessageAdapter.MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(viewType == RIGHT_VIEW ? R.layout.message_right : R.layout.message_left,
                        parent,false);
        return new MessageAdapter.MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageAdapter.MessageViewHolder holder, int position) {
        Message message = getMessages().get(position);

        String username = message.getSender().getFirstName();
        String text = message.getText();
        String date = "";

        Date created = message.getCreated();
        if(created != null) {
            date = DATE_FORMAT.format(created);
        }

        holder.username.setText(username);
        holder.text.setText(text);
        holder.date.setText(date);

        if(message.getPhotos().size() > 0) {
            Photo photo = message.getPhotos().get(0);
            Picasso.get()
                   .load(Uri.parse(ChatService.GET_IMAGE_URL + photo.getId() + "?width=" + 400))
                   .into(holder.image);

        } else {
            holder.image.setImageResource(android.R.color.transparent);
        }
    }

    @Override
    public int getItemCount() {
        return getMessages().size();
    }


    interface OnClickListener {
        void onClick(int position);
    }


    class MessageViewHolder extends RecyclerView.ViewHolder {

        TextView username;
        TextView text;
        TextView date;
        ImageView image;

        public MessageViewHolder(View view) {
            super(view);
            view.setOnClickListener(v -> listener.onClick(getAdapterPosition()));
            this.username = view.findViewById(R.id.username);
            this.text = view.findViewById(R.id.text);
            this.date = view.findViewById(R.id.date);
            this.image = view.findViewById(R.id.image);
        }
    }
}
