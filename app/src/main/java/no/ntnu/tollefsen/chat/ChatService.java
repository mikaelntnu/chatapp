package no.ntnu.tollefsen.chat;

import android.util.JsonReader;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import no.ntnu.tollefsen.chat.domain.Conversation;
import no.ntnu.tollefsen.chat.domain.Message;
import no.ntnu.tollefsen.chat.domain.Photo;
import no.ntnu.tollefsen.chat.domain.User;

public class ChatService {
    private static ChatService SINGLETON;

    public static final String DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ss.S'Z'[z]";

    //public static final String IP = "158.38.66.137";
    public static final String IP = "192.168.1.85";
    public static final String APIURL = "http://" + IP + ":8080/api/";
    public static final String LOGIN_URL = APIURL + "auth/login";
    public static final String CURRENT_USER_URL = APIURL + "auth/currentuser";
    public static final String CONVERSATION_URL = APIURL + "conversation";
    public static final String CREATE_CONVERSATION_URL = APIURL + "conversation/createconversation";
    public static final String SEND_MESSAGE_URL = APIURL + "chat/send";
    public static final String GET_IMAGE_URL = APIURL + "chat/image/";
    public static final String GET_USERS_URL = APIURL + "chat/users";

    public static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(DATE_FORMAT_PATTERN);

    /** JWT token returned from login */
    String token;

    public static synchronized ChatService getSingleton() {
        if(SINGLETON == null) {
            SINGLETON = new ChatService();
        }

        return SINGLETON;
    }

    public interface OnConversationsLoaded {
        void onLoaded(List<Conversation> conversations);
    }


    protected User user;
    protected OnConversationsLoaded onConversationsLoaded = conversations -> {};
    protected Conversation selectedConversation;
    protected List<Conversation> conversations = new ArrayList<>();

    public void setConversationsLoadedListener(OnConversationsLoaded listener) {
        this.onConversationsLoaded = listener;
    }

    public Conversation getSelectedConversation() {
        return selectedConversation;
    }

    public void setSelectedConversation(Long conversationId) {
        if(conversationId != null) {
            conversations.stream()
                .filter(conversation -> conversation.getId() == conversationId)
                .findFirst().ifPresent(this::setSelectedConversation);
        }
    }

    public void setSelectedConversation(Conversation selectedConversation) {
        this.selectedConversation = selectedConversation;
    }

    public List<Conversation> getConversations() {
        return conversations;
    }


    public HttpURLConnection getSecureConnection(String url) throws IOException {
        HttpURLConnection result = (HttpURLConnection) new URL(url).openConnection();
        result.setRequestProperty("Authorization", "Bearer " + token);
        result.setConnectTimeout(3000);
        return result;
    }

    public User login(String userid, String password) throws IOException {
        HttpURLConnection con = null;
        try {
            URL url = new URL(LOGIN_URL + "?uid=" + userid + "&pwd=" + password);
            con = (HttpURLConnection) url.openConnection();
            con.setConnectTimeout(3000);
            token = new BufferedReader(new InputStreamReader(con.getInputStream())).readLine();
            user = getCurrentUser();
            con.getInputStream().close(); // Why?
        } finally {
            if(con != null) con.disconnect();
        }

        return user;
    }

    public User getCurrentUser() throws IOException {
        HttpURLConnection con = null;
        try {
            con = getSecureConnection(CURRENT_USER_URL);
            JsonReader jr = new JsonReader(new InputStreamReader(new BufferedInputStream(con.getInputStream())));
            user = loadUser(jr);
            con.getInputStream().close(); // Why?
        } finally {
            if(con != null) con.disconnect();
        }

        return user;
    }

    public boolean isUser(String userid) {
        if(userid == null || user == null)
            return  false;

        return userid.compareTo(user.getUserid()) == 0;
    }


    public List<User> loadAllUsers() throws IOException {
        List<User> result;

        HttpURLConnection con = getSecureConnection(GET_USERS_URL);
        try(JsonReader jr = new JsonReader(new InputStreamReader(new BufferedInputStream(con.getInputStream())))) {
            result = loadUsers(jr);
            con.getInputStream().close(); // Why?
        } finally {
            con.disconnect();
        }

        return result;
    }



    public Conversation createConversation(List<User> recipients) throws IOException {
        Conversation result;

        HttpURLConnection con = getSecureConnection(CREATE_CONVERSATION_URL);
        con.setDoOutput( true );
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        con.setUseCaches( false );

        // Write list of recipients
        try(BufferedWriter bos = new BufferedWriter(new OutputStreamWriter(con.getOutputStream(), StandardCharsets.UTF_8))) {
            for (int i = 0; i < recipients.size(); i++) {
                User user = recipients.get(i);
                if(i > 0) {
                    bos.write('&');
                }

                bos.write("recipientids");
                bos.write('=');
                bos.write(URLEncoder.encode(user.getUserid(),"UTF-8"));
            }
        }

        // Read JSON result
        try(JsonReader jr = new JsonReader(new InputStreamReader(new BufferedInputStream(con.getInputStream())))) {
            result = loadConversation(jr);
            con.getInputStream().close(); // Why?
        } finally {
            con.disconnect();
        }

        conversations.add(0,result);
        setSelectedConversation(result);
        onConversationsLoaded.onLoaded(conversations);

        return result;
    }


    public List<Conversation> loadConversations() throws IOException {
        List<Conversation> result = new ArrayList<>();

        HttpURLConnection c = null;
        try {
            c = getSecureConnection(CONVERSATION_URL);
            c.setUseCaches(true);
            c.setRequestMethod("GET");

            if(c.getResponseCode() == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(c.getInputStream(), StandardCharsets.UTF_8));
                JsonReader jr = new JsonReader(br);
                result = loadConversations(jr);
                jr.close();
                c.getInputStream().close(); // Why?
            }
        } finally {
            if(c != null) c.disconnect();
        }

        conversations = new ArrayList<>(result);
        onConversationsLoaded.onLoaded(conversations);

        return result;
    }

    private List<Conversation> loadConversations(JsonReader jr) throws IOException {
        List<Conversation> result = new ArrayList<>();

        jr.beginArray();
        while(jr.hasNext()) {
            result.add(loadConversation(jr));
        }
        jr.endArray();

        return result;
    }

    private Conversation loadConversation(JsonReader jr) throws IOException {
        Conversation result = new Conversation();

        jr.beginObject();
        while (jr.hasNext()) {
            switch (jr.nextName()) {
                case "id":
                    result.setId(jr.nextLong());
                    break;
                case "messages":
                    result.setMessages(loadMessages(jr));
                    break;
                case "recipients":
                    result.setRecipients(loadUsers(jr));
                    break;
                default:
                    jr.skipValue();
            }
        }
        jr.endObject();

        return result;
    }

    private List<Message> loadMessages(JsonReader jr) throws IOException {
        List<Message> result = new ArrayList<>();

        jr.beginArray();
        while(jr.hasNext()) {
            result.add(loadMessage(jr));
        }
        jr.endArray();

        return result;
    }


    public Message loadMessage(JsonReader jr) throws IOException {
        Message result = new Message();

        jr.beginObject();
        while (jr.hasNext()) {
            switch (jr.nextName()) {
                case "id":
                    result.setId(jr.nextLong());
                    break;
                case "conversation":
                    result.setConversationId(jr.nextLong());
                    break;
                case "sender":
                    result.setSender(loadUser(jr));
                    break;
                case "text":
                    result.setText(jr.nextString());
                    break;
                case "created":
                    try {
                        result.setCreated(DATE_FORMAT.parse(jr.nextString()));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    break;
                case "photos":
                    jr.beginArray();
                    while(jr.hasNext()) {
                        result.getPhotos().add(new Photo(jr.nextString()));
                    }
                    jr.endArray();
                    break;
                default:
                    jr.skipValue();
            }
        }
        jr.endObject();

        return result;
    }

    private List<User> loadUsers(JsonReader jr) throws IOException {
        List<User> result = new ArrayList<>();

        jr.beginArray();
        while(jr.hasNext()) {
            result.add(loadUser(jr));
        }
        jr.endArray();

        return result;
    }


    private User loadUser(JsonReader jr) throws IOException {
        User result = new User();

        jr.beginObject();
        while (jr.hasNext()) {
            switch (jr.nextName()) {
                case "userid":
                    result.setUserid(jr.nextString());
                    break;
                case "firstName":
                    result.setFirstName(jr.nextString());
                    break;
                case "lastName":
                    result.setLastName(jr.nextString());
                    break;
                default:
                    jr.skipValue();
            }
        }
        jr.endObject();

        return result;
    }
}
