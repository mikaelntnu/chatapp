package no.ntnu.tollefsen.chat;

import android.content.SearchRecentSuggestionsProvider;
import android.util.Log;


public class MessageSuggestionProvider extends SearchRecentSuggestionsProvider {
    public static final String AUTHORITY = "no.ntnu.tollefsen.chat.MessageSuggestionProvider";
    public static final int MODE = DATABASE_MODE_QUERIES;  // Configures database to record recent queries

    public MessageSuggestionProvider() {
        Log.i("Search","MessageSuggestionProvider");
        setupSuggestions(AUTHORITY,MODE);
    }
}
