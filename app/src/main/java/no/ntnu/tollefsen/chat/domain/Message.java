package no.ntnu.tollefsen.chat.domain;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Message {
    long id;
    User sender;
    List<Photo> photos = new ArrayList<>();
    String text;
    Date created;

    Long conversationId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public void addPhoto(Photo photo) {
        getPhotos().add(photo);
    }

    public void addPhoto(File photo) {
        getPhotos().add(new Photo(photo));
    }

    public List<Photo> getPhotos() {
        return photos;
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
    }

    public String getText() {
        return text != null ? text : "";
    }

    public void setText(String text) {
        this.text = text;
    }

    public Long getConversationId() {
        return conversationId;
    }

    public void setConversationId(Long conversationId) {
        this.conversationId = conversationId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
