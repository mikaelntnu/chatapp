package no.ntnu.tollefsen.chat;

import java.util.List;

import no.ntnu.tollefsen.chat.domain.User;


/**
 * Random methods with no proper home.
 */
public class Tools {
    static final int MAX_USERS = 3;

    public static final String getRecipients(List<User> users) {
        StringBuilder builder = new StringBuilder();
        int size = users.size();
        for (int i = 0; i < MAX_USERS && i < size; i++) {
            User user = users.get(i);
            if(i > 0) {
                builder.append(", ");
            }
            builder.append(user.getFirstName());
        }
        if(size > MAX_USERS) {
            builder.append(", og ")
                    .append(size-MAX_USERS)
                    .append(" til");
        }

        return builder.toString();
    }
}
