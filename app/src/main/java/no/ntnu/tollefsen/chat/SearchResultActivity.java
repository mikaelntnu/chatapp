package no.ntnu.tollefsen.chat;

import android.app.SearchManager;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SearchRecentSuggestions;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import no.ntnu.tollefsen.chat.domain.Conversation;
import no.ntnu.tollefsen.chat.domain.Message;

public class SearchResultActivity extends AppCompatActivity {
    private MessageAdapter messageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_result);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        messageAdapter = new MessageAdapter();
        messageAdapter.setOnClickListener(item -> {
            Long conversationId = messageAdapter.getMessages().get(item).getConversationId();
            ChatService.getSingleton().setSelectedConversation(conversationId);
            startActivity(new Intent(this, MessageActivity.class));
        });

        RecyclerView recyclerView = findViewById(R.id.messages);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(messageAdapter);
        recyclerView.addItemDecoration(new MarginDecoration(10));

        Intent intent = getIntent();
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            SearchRecentSuggestions suggestions = new SearchRecentSuggestions(this,
                    MessageSuggestionProvider.AUTHORITY, MessageSuggestionProvider.MODE);
            suggestions.saveRecentQuery(query, null);
            doSearch(query);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    /**
     *
     * @param query
     */
    private void doSearch(String query) {
        query = query.toLowerCase();

        List<Message> result = new ArrayList<>();
        for(Conversation item : ChatService.getSingleton().getConversations()) {
            for(Message message : item.getMessages()) {
                if (message.getText().toLowerCase().contains(query)) {
                    result.add(message);
                }
            }
        }

        messageAdapter.setMessages(result);
    }
}
