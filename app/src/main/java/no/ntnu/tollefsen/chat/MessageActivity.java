package no.ntnu.tollefsen.chat;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.FileProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import no.ntnu.tollefsen.chat.domain.Conversation;
import no.ntnu.tollefsen.chat.domain.Message;
import no.ntnu.tollefsen.chat.tasks.PostMessageTask;

public class MessageActivity extends AppCompatActivity {
    public static final String EXTRA_CONVERSATION = "no.ntnu.tollefsen.chat.CONVERSATION";
    static final String FILEPROVIDER = "no.ntnu.tollefsen.chat.fileprovider";

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_GALLERY = 2;

    RecyclerView recyclerView;
    MessageAdapter adapter = new MessageAdapter();
    List<File> photoFiles = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        FloatingActionButton fab = findViewById(R.id.fabcam);
        fab.setOnClickListener(view -> {
            System.out.println("extenal path: " + Environment.getExternalStorageDirectory().getAbsolutePath());
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (intent.resolveActivity(getPackageManager()) != null) {
                File file = createImageFile();
                try {
                    System.out.println("Can crate" + file.createNewFile());
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (file != null) {
                    Uri photoURI = FileProvider.getUriForFile(this, FILEPROVIDER, file);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                    photoFiles.add(file);
                    startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
                }
            }
        });

        recyclerView = findViewById(R.id.messages);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setStackFromEnd(true); // Keep scroll-position when displaying keyboard
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new MarginDecoration(10));

        Conversation conversation = ChatService.getSingleton().getSelectedConversation();
        if(conversation != null) {
            // Add recipients to view
            TextView recipients = findViewById(R.id.recipients);
            recipients.setText(Tools.getRecipients(conversation.getRecipients()));

            adapter.setMessages(conversation.getMessages());
            recyclerView.post(() -> recyclerView.scrollToPosition(adapter.getItemCount()-1));
        }

        findViewById(R.id.send).setOnClickListener(this::sendMessage);
    }



    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    /**
     *
     * @return
     * @throws IOException
     */
    private File createImageFile() {
        File result = null;

        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);

        try {
            result = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",   /* suffix */
                    storageDir      /* directory */
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        return result;
    }


    private void sendMessage(View view) {
        EditText text = findViewById(R.id.message);
        Message message = new Message();
        message.setText(text.getText().toString());
        message.setConversationId(ChatService.getSingleton().getSelectedConversation().getId());
        photoFiles.stream()
                  .filter(file -> file.length() > 0)
                  .forEach(message::addPhoto);

        new PostMessageTask(messages -> {
            // Cleanup after posting message

            adapter.addMessages(messages); // Add message to adapter
            text.getText().clear(); // Clear text

            // Remove temporary files
            photoFiles.forEach(File::delete);
            photoFiles.clear();

            // Hide keyboard
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(text.getWindowToken(),0);

            // Scroll RecyclerView to bottom
            if(adapter.getItemCount() > 0) {
                recyclerView.post(() -> recyclerView.smoothScrollToPosition(adapter.getItemCount() - 1));
            }
        },this::onException).execute(message);
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            try {
                if(requestCode == REQUEST_IMAGE_GALLERY) {
                    //executePost(getContentResolver().openInputStream(data.getData()));
                } else if(requestCode == REQUEST_IMAGE_CAPTURE) {
                    if(photoFiles.size() > 0 && photoFiles.get(photoFiles.size()-1).length() > 0) {
                        //executePost(new FileInputStream(currentPhotoPath));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }


    /**
     * Display errormessage to user
     *
     * @param throwable
     */
    protected void onException(Throwable throwable) {
        Snackbar.make(findViewById(android.R.id.content),
                "Failed to communicate with server", Snackbar.LENGTH_LONG)
                .setAction("Action", null)
                .show();
    }
}
